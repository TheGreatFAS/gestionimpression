<?php

use Illuminate\Database\Seeder;
use App\Models\Role;
use App\Models\Permission;

class RoleTableSeeder extends Seeder
{

    public function run()
    {

        DB::table('Roles')->delete();
        $role = new Role();
        $role->name         = 'admin';
        $role->display_name = 'Administrateur du système';
        $role->description  = 'Cet usager a tous les droits sur le système, c\'est le super admin';
        $role->save();

        $permission = Permission::where('name', '=', 'gestion-imprimante')->first();
        $role->attachPermission($permission);
        $permission = Permission::where('name', '=', 'utiliser-imprimante')->first();
        $role->attachPermission($permission);

        $role = new Role();
        $role->name         = 'utilisateur';
        $role->display_name = 'Utilisateur du système';
        $role->description  = 'Cet usager peut utiliser les imprimantes, mais pas les modifier';
        $role->save();

        $permission = Permission::where('name', '=', 'utiliser-imprimante')->first();
        $role->attachPermission($permission);
    }
}