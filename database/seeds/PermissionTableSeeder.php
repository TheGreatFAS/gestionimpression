<?php

use Illuminate\Database\Seeder;
use App\Models\Permission;


class PermissionTableSeeder extends Seeder
{

    public function run()
    {

        DB::table('Permissions')->delete();
        $permission = new Permission();
        $permission->name         = 'gestion-imprimante';
        $permission->display_name = 'Gérer les imprimantes';
        $permission->description  = 'Permet de modifier la config des imprimantes';
        $permission->save();

        $permission = new Permission();
        $permission->name         = 'utiliser-imprimante';
        $permission->display_name = 'Utiliser les imprimantes';
        $permission->description  = 'Permet d\'utiliser les imprimantes';
        $permission->save();
    }
}